package com.thoughtworks.vapasi.valueObjects.test;

import com.thoughtworks.vapasi.valueObjects.Rupee;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class RupeeTest {

    @Test
    public void shouldCheckIfTwoValuesAreSame() {
        assertEquals(true, new Rupee(10.0).equals(new Rupee(10.0)));
    }

    @Test
    public void shouldCheckIfTwoValuesAreNotSame() {
        assertEquals(false, new Rupee(10.0).equals(new Rupee(5.0)));
    }

    @Test
    public void shouldCheckIfRupeeIsNull() {
        assertEquals(false, new Rupee(10.0).equals(null));
    }

    @Test
    public void shouldCheckIfSumOfTwoRupeesEqualToAnother() {
        assertEquals(true,new Rupee(10.0).equals(new Rupee(3.0).addRupee(new Rupee(7.0))));
    }

    @Test
    public void shouldCheckIfSumOfTwoRupeesNotEqualToAnother() {
        assertEquals(false,new Rupee(15.0).equals(new Rupee(3.0).addRupee(new Rupee(7.0))));
    }

}
