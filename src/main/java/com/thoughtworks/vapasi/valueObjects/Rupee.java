package com.thoughtworks.vapasi.valueObjects;

import java.util.Objects;

public class Rupee {

    private Double value;

    public Rupee (Double value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Rupee rupee = (Rupee) o;
        return Objects.equals(value, rupee.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }

    public Rupee addRupee(Rupee rupee) {
        return new Rupee(this.value + rupee.value);
    }
}
